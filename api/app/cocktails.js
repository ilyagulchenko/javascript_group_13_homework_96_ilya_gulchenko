const express = require('express');
const multer = require("multer");
const config = require("../config");
const { nanoid } = require("nanoid");
const path = require("path");
const Cocktail = require('../models/Cocktail');
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");
const mongoose = require("mongoose");
const User = require("../models/User");

const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

router.get("/", async (req, res, next) => {
    try {
        const query = {};

        if (req.query.user) {
            query.user = req.query.user;
        }

        const cocktails = await Cocktail.find(query).populate('user', 'displayName');

        return res.send(cocktails);
    } catch (e) {
        next(e);
    }
});

router.get("/:id", async (req, res, next) => {
    try {
        const cocktail = await Cocktail.findById(req.params.id);

        if (!cocktail) {
            return res.status(404).send({message: 'Not found!'});
        }

        newUser = await User.findById(cocktail.user);
        cocktail.user = newUser;

        return res.send(cocktail);
    } catch (e) {
        next(e);
    }
});

router.post("/", auth, upload.single('image'), async (req, res, next) => {
    try {
        const cocktailData = {
            user: req.user,
            title: req.body.title,
            recipe: req.body.recipe,
            ingredients: req.body.ingredients,
            image: null,
        };

        if (req.file) {
            cocktailData.image = req.file.filename;
        }

        if (req.user.role === 'admin') {
            cocktailData.is_published = true;
        }

        const cocktail = new Cocktail(cocktailData);

        await cocktail.save();

        return res.send(cocktail);
    } catch (e) {
        next(e);
    }
});

router.put('/:id', auth, permit('admin'), async (req, res, next) => {
    try {
        const cocktail = await Cocktail.findById(req.params.id);

        if (!cocktail) {
            return res.status(404).send({error: 'Cocktail not found'});
        }

        cocktail.is_published = true;

        await cocktail.save();

        return res.send(cocktail);
    } catch (error) {
        if (error instanceof mongoose.Error.ValidationError) {
            return res.status(400).send(error);
        }

        return next(error);
    }
});

router.delete("/:id", auth, permit('admin'), async (req, res, next) => {
    try {
        await Cocktail.deleteOne({_id: req.params.id});

        return res.send({message: 'Cocktail deleted'});
    } catch (e) {
        next(e);
    }
});

module.exports = router;
