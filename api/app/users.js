const express = require('express');
const config = require('../config');
const User = require('../models/User');
const axios = require("axios");
const fs = require('fs');
const fetch = require("node-fetch");

const router = express.Router();

router.post('/sessions', async (req,res,next) => {
    try {
        const user = await User.findOne({displayName: req.body.displayName});

        if (!user) {
            return res.status(400).send({error: 'Name not found'});
        }

        user.generateToken();
        await user.save();

        return res.send(user);
    } catch (e) {
        next(e);
    }
});

router.post('/facebookLogin', async (req, res, next) => {
    try {
        const inputToken = req.body.authToken;
        const accessToken = config.facebook.appId + '|' + config.facebook.appSecret;
        const debugTokenUrl = `https://graph.facebook.com/debug_token?input_token=${inputToken}&access_token=${accessToken}`;

        const response = await axios.get(debugTokenUrl);

        if (response.data.data.error) {
            return res.status(401).send({message: 'Facebook token incorrect'});
        }

        if (req.body.id !== response.data.data.user_id) {
            return res.status(401).send({message: 'Wrong User ID'});
        }

        let user = await User.findOne({facebookId: req.body.id});

        if (!user) {
            const imageUrl = req.body.avatar;

            fetch(imageUrl)
                .then(res =>
                    res.body.pipe(fs.createWriteStream('./public/uploads/avatar.jpeg'))
                )

            user = new User({
                facebookId: req.body.id,
                displayName: req.body.name,
                avatar: 'avatar.jpeg'
            });
        }

        user.generateToken();
        await user.save();

        return res.send(user);
    } catch (e) {
        next(e);
    }
});

router.delete('/sessions', async(req,res,next) => {
    try {
        const token = req.get('Authorization');
        const message = {message: 'OK'};

        if (!token) {
            return res.send(message);
        }

        const user = await User.findOne({token});

        if (!user) {
            return res.send(message);
        }

        user.generateToken();
        await user.save();

        return res.send(message);
    } catch (e) {
        next(e);
    }
});

module.exports = router;
