const path = require('path');

const rootPath = __dirname;

module.exports = {
    rootPath,
    uploadPath: path.join(rootPath, 'public/uploads'),
    mongoConfig: {
        db: 'mongodb://localhost/cocktails',
        options: {useNewUrlParser: true},
    },
    facebook: {
        appId: '3067484406848024',
        appSecret: '221c898e4db8fac6fcb28709f9bdc06f'
    }
}
