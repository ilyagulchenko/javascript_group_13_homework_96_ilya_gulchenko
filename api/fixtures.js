const mongoose = require('mongoose');
const config = require('./config');
const Cocktail = require("./models/Cocktail");
const User = require("./models/User");
const { nanoid } = require("nanoid");

const run = async () => {
    await mongoose.connect(config.mongoConfig.db, config.mongoConfig.options);

    const collections = await mongoose.connection.db.listCollections().toArray();

    for (const coll of collections) {
        await mongoose.connection.db.dropCollection(coll.name);
    }

    const [admin, user] = await User.create({
        role: 'admin',
        displayName: 'admin',
        token: nanoid(),
        avatar: 'userOne.png'
    }, {
        role: 'user',
        displayName: 'user',
        token: nanoid(),
        avatar: 'userOne.png'
    });

    await Cocktail.create({
        user: admin,
        title: 'Whiskey & Coke',
        image: 'wiskey&coke.jpg',
        recipe: 'Add the whiskey into a double rocks glass over one large ice cube or a highball glass filled with ice. Top with the cola and stir briefly and gently to combine.',
        ingredients: [{
            title: 'whiskey',
            quantity: '2 ounces'
        }, {
            title: 'Coca-Cola',
            quantity: '4 to 6 ounces'
        }, {
            title: 'Lemon',
            quantity: '1 slice'
        }]
    }, {
        user: user,
        title: 'Отвертка',
        image: 'отвертка.jpg',
        recipe: 'Налить немного водки, смешав ее вместе с апельсиновым соком.',
        is_published: true,
        ingredients: [{
            title: 'Водка',
            quantity: '50 мл.'
        }, {
            title: 'Апельсиновый сок',
            quantity: '150 мл.'
        }, {
            title: 'Апельсин',
            quantity: '30 г.'
        }]
    });

    await mongoose.connection.close();
};

run().catch(e => console.error(e));
