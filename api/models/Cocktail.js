const mongoose = require('mongoose');

const CocktailSchema = new mongoose.Schema({
    user: {
       type: mongoose.Schema.Types.ObjectId,
       ref: 'User',
       required: true,
    },
    title: {
        type: String,
        required: true,
    },
    image: String,
    recipe: String,
    is_published: {
        type: Boolean,
        default: false,
    },
    ingredients: [{
        title: String,
        quantity: String
    }],
});

const Cocktail = mongoose.model('Cocktail', CocktailSchema);

module.exports = Cocktail;
