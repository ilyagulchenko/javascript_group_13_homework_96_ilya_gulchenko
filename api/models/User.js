const mongoose = require('mongoose');
const { nanoid } = require('nanoid');

const UserSchema = new mongoose.Schema({
    facebookId: String,
    role: {
        type: String,
        required: true,
        default: 'user',
        enum: ['user', 'admin']
    },
    displayName: String,
    token: {
        type: String,
        required: true,
    },
    avatar : String,
});

UserSchema.methods.generateToken = function () {
    this.token = nanoid();
};

const User = mongoose.model('User', UserSchema);

module.exports = User;
