import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { CocktailsComponent } from './pages/cocktails/cocktails.component';
import { UserCocktailsComponent } from './pages/cocktails/user-cocktails/user-cocktails.component';
import { EditCocktailComponent } from './pages/cocktails/edit-cocktail/edit-cocktail.component';
import { CocktailDetailsComponent } from './pages/cocktails/cocktail-details/cocktail-details.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'cocktails', component: CocktailsComponent},
  {path: 'cocktails/new', component: EditCocktailComponent},
  {path: 'cocktails/:id', component: CocktailDetailsComponent},
  {path: 'my-cocktails', component: UserCocktailsComponent},
  {path: 'login', component: LoginComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
