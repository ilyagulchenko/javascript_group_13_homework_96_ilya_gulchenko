export interface Cocktail {
  _id: string,
  user: {
    _id: string,
    displayName: string
  },
  title: string,
  image: string,
  recipe: string,
  is_published: boolean,
  ingredients: Ingredients[]
}

export interface User {
  _id: string,
  displayName: string,
  avatar: string
}

export interface CocktailId {
  _id: string,
  user: User,
  title: string,
  image: string,
  recipe: string,
  is_published: boolean,
}

export interface Ingredients {
  title: string,
  quantity: string
}

export interface CocktailDataApi {
  _id: string,
  title: string,
  image: string,
  recipe: string,
  is_published: boolean
}

export interface CocktailData {
  [key: string]: any;
  user: string;
  title: string;
  image: File;
  recipe: string;
  ingredients: Ingredients[];
}
