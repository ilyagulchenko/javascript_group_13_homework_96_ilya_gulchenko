export interface User {
  _id: string,
  role: string,
  displayName: string,
  token: string,
  avatar: string,
}

export interface LoginFacebookUser {
  id: string,
  authToken: string,
  email: string,
  name: string,
  avatar: string
}

export interface LoginUserData {
  displayName: string
}

export interface LoginError {
  error: string
}
