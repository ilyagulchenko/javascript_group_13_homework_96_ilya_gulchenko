import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { CocktailId } from '../../../models/cocktail.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../../store/types';
import { getCocktailRequest } from '../../../store/cocktail/cocktail.actions';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-cocktail-details',
  templateUrl: './cocktail-details.component.html',
  styleUrls: ['./cocktail-details.component.sass']
})
export class CocktailDetailsComponent implements OnInit {
  cocktail: Observable<null | CocktailId>;

  constructor(private store: Store<AppState>, private route: ActivatedRoute) {
    this.cocktail = store.select(state => state.cocktail.cocktail);
  }

  ngOnInit(): void {
    // @ts-ignore
    const id: string = this.route.params.value.id
    this.store.dispatch(getCocktailRequest({id}));
  }

}
