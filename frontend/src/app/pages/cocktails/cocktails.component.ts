import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Cocktail } from '../../models/cocktail.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import {
  fetchCocktailsRequest,
  publishCocktailRequest,
  removeCocktailRequest
} from '../../store/cocktails/cocktails.actions';
import { User } from '../../models/user.model';

@Component({
  selector: 'app-cocktails',
  templateUrl: './cocktails.component.html',
  styleUrls: ['./cocktails.component.sass']
})
export class CocktailsComponent implements OnInit {
  cocktails: Observable<Cocktail[]>;
  user: Observable<null | User>;
  loading: Observable<boolean>;
  error: Observable<null | string>;
  role = '';

  constructor(private store: Store<AppState>) {
    this.cocktails = store.select(state => state.cocktails.cocktails);
    this.loading = store.select(state => state.cocktails.fetchLoading);
    this.error = store.select(state => state.cocktails.fetchError);
    this.user = store.select(state => state.users.user);
    this.user.subscribe(user => {
      if (user) {
        this.role = user.role;
      }
    });
  }

  ngOnInit(): void {
    this.store.dispatch(fetchCocktailsRequest());
  }

  isAdmin() {
    if (this.role === 'admin') {
      return true;
    }
    return false;
  }

  publishCocktail(id: string, change: {is_published: true}) {
    this.store.dispatch(publishCocktailRequest({id, change}));
  }

  removeCocktail(id: string) {
    this.store.dispatch(removeCocktailRequest({id}));
  }
}
