import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from '../../../store/types';
import { CocktailData } from '../../../models/cocktail.model';
import { createCocktailRequest } from '../../../store/cocktails/cocktails.actions';

@Component({
  selector: 'app-edit-cocktail',
  templateUrl: './edit-cocktail.component.html',
  styleUrls: ['./edit-cocktail.component.sass']
})
export class EditCocktailComponent implements OnInit{
  @ViewChild('f') form!: NgForm;
  cocktailForm!: FormGroup;
  ingredients = [];

  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {
    this.cocktailForm = new FormGroup({
      ingredients: new FormArray([]),
    })
  }

  save() {
    const cocktailData: CocktailData = this.form.value;
    for (let value of this.cocktailForm.value.ingredients){
      let obj = {
        title: value.ingredientName,
        quantity: value.amount
      }
      // @ts-ignore
      this.ingredients.push(obj);
    }

    const newCocktail = {
      title: cocktailData.title,
      recipe: cocktailData.recipe,
      image: cocktailData.image,
      ingredients: this.ingredients
    }

    console.log(newCocktail)
  }

  addIngredient() {
    const ingredients = <FormArray>this.cocktailForm.get('ingredients');
    const ingredientGroup = new FormGroup({
      ingredientName: new FormControl('', Validators.required),
      amount: new FormControl('', Validators.required),
      measurability: new FormControl('', Validators.required)
    });
    ingredients.push(ingredientGroup);
  }

  getIngredientControls() {
    const ingredients = <FormArray>this.cocktailForm.get('ingredients');
    return ingredients.controls;
  }

  removeIngredient(id: any) {
    const field = <FormArray>this.cocktailForm.get('ingredients');
    field.removeAt(id);
  }

  createCocktail() {
    const cocktailData: CocktailData = this.form.value;
    this.store.dispatch(createCocktailRequest({cocktailData}));
  }

}
