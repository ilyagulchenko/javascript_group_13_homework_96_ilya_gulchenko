import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../../store/types';
import { Observable } from 'rxjs';
import { CocktailDataApi } from '../../../models/cocktail.model';
import { fetchUserCocktailsRequest } from '../../../store/cocktails/cocktails.actions';
import { User } from '../../../models/user.model';

@Component({
  selector: 'app-user-cocktails',
  templateUrl: './user-cocktails.component.html',
  styleUrls: ['./user-cocktails.component.sass']
})
export class UserCocktailsComponent implements OnInit {
  userCocktails: Observable<CocktailDataApi[]>;
  user: Observable<null | User>;
  id = '';
  role = '';

  constructor(private store: Store<AppState>) {
    this.userCocktails = store.select(state => state.cocktails.userCocktails);
    this.user = store.select(state => state.users.user);
    this.user.subscribe(user => {
      if (user) {
        this.id = user._id;
        this.role = user.role;
      }
    });
  }

  ngOnInit(): void {
    const id = this.id;
    this.store.dispatch(fetchUserCocktailsRequest({id}));
  }

  isUser() {
    if (this.role === 'user') {
      return true;
    }
    return false;
  }

}
