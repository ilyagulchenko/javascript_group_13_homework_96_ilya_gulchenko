import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Cocktail, CocktailData, CocktailId } from '../models/cocktail.model';
import { environment as env } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CocktailsService {
  constructor(private http: HttpClient) {}

  getCocktails() {
    return this.http.get<Cocktail[]>(env.apiUrl + '/cocktails');
  }

  getUserCocktails(id: string) {
    return this.http.get<Cocktail[]>(env.apiUrl + `/cocktails?user=${id}`);
  }

  getCocktail(id: string) {
    return this.http.get<CocktailId>(env.apiUrl + '/cocktails/' + id);
  }

  createCocktail(cocktailData: CocktailData) {
    const formData = new FormData();

    Object.keys(cocktailData).forEach(key => {
      if (cocktailData[key] !== null) {
        formData.append(key, cocktailData[key]);
      }
    });

    return this.http.post(env.apiUrl + '/cocktails', formData);
  }

  publishCocktail(id: string, change: {is_published: true}) {
    return this.http.put<Cocktail>(env.apiUrl + '/cocktails/' + id, change);
  }

  removeCocktail(id: string) {
    return this.http.delete(env.apiUrl + `/cocktails/${id}`);
  }
}
