import { ActionReducer, MetaReducer, StoreModule } from '@ngrx/store';
import { localStorageSync } from 'ngrx-store-localstorage';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { usersReducer } from './users/users.reducer';
import { UsersEffects } from './users/users.effects';
import { CocktailsEffects } from './cocktails/cocktails.effects';
import { cocktailsReducer } from './cocktails/cocktails.reducer';
import { cocktailReducer } from './cocktail/cocktail.reducer';
import { CocktailEffects } from './cocktail/cocktail.effects';

const localStorageSyncReducer = (reducer: ActionReducer<any>) => {
  return localStorageSync({
    keys: [{users: ['user']}],
    rehydrate: true
  })(reducer);
}

const metaReducers: MetaReducer[] = [localStorageSyncReducer];

const reducers = {
  users: usersReducer,
  cocktails: cocktailsReducer,
  cocktail: cocktailReducer
}

const effects = [UsersEffects, CocktailsEffects, CocktailEffects]

@NgModule({
  imports: [
    StoreModule.forRoot(reducers, {metaReducers}),
    EffectsModule.forRoot(effects),
  ]
})
export class AppStoreModule {}

