import { createAction, props } from '@ngrx/store';
import { CocktailId } from '../../models/cocktail.model';

export const getCocktailRequest = createAction('[Cocktail] Get Request', props<{id: string}>());
export const getCocktailSuccess = createAction('[Cocktail] Get Success', props<{cocktail: CocktailId}>());
export const getCocktailFailure = createAction('[Cocktail] Get Failure', props<{error: string}>());
