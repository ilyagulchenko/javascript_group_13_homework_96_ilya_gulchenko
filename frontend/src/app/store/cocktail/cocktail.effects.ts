import { map, mergeMap } from 'rxjs';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { CocktailsService } from '../../services/cocktails.service';
import { getCocktailRequest, getCocktailSuccess } from './cocktail.actions';

@Injectable()
export class CocktailEffects {
  constructor(
    private actions: Actions,
    private cocktailsService: CocktailsService,
  ) {}

  getCocktail = createEffect(() => this.actions.pipe(
    ofType(getCocktailRequest),
    mergeMap(({id}) => this.cocktailsService.getCocktail(id).pipe(
      map(cocktail => getCocktailSuccess({cocktail}))
    ))
  ));
}
