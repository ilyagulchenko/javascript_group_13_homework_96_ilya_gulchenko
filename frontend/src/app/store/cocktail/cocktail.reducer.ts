import { CocktailState } from '../types';
import { createReducer, on } from '@ngrx/store';
import { getCocktailFailure, getCocktailRequest, getCocktailSuccess } from './cocktail.actions';

const initialState: CocktailState = {
  cocktail: null,
  getLoading: false,
  getError: null
}

export const cocktailReducer = createReducer(
  initialState,
  on(getCocktailRequest, state => ({...state, getLoading: true})),
  on(getCocktailSuccess, (state, {cocktail}) => ({...state, fetchLoading: false, cocktail})),
  on(getCocktailFailure, (state, {error}) => ({...state, fetchLoading: false, getError: error})),
);
