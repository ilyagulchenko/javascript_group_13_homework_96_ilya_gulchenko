import { createAction, props } from '@ngrx/store';
import { Cocktail, CocktailData, CocktailDataApi } from '../../models/cocktail.model';

export const fetchCocktailsRequest = createAction('[Cocktails] Fetch Request');
export const fetchCocktailsSuccess = createAction('[Cocktails] Fetch Success', props<{cocktails: Cocktail[]}>());
export const fetchCocktailsFailure = createAction('[Cocktails] Fetch Failure', props<{error: string}>());

export const fetchUserCocktailsRequest = createAction('[UserCocktails] Fetch Request', props<{id: string}>());
export const fetchUserCocktailsSuccess = createAction('[UserCocktails] Fetch Success', props<{userCocktails: CocktailDataApi[]}>());
export const fetchUserCocktailsFailure = createAction('[UserCocktails] Fetch Failure', props<{error: string}>());

export const createCocktailRequest = createAction('[Cocktail] Create Request', props<{cocktailData: CocktailData}>());
export const createCocktailSuccess = createAction('[Cocktail] Create Success');
export const createCocktailFailure = createAction('[Cocktail] Create Failure', props<{error: string}>());

export const publishCocktailRequest = createAction('[Cocktail] Publish Request', props<{id: string, change: {is_published: true}}>());
export const publishCocktailSuccess = createAction('[Cocktail] Publish Success', props<{cocktail: Cocktail}>());
export const publishCocktailFailure = createAction('[Cocktail] Publish Failure', props<{error: string}>());

export const removeCocktailRequest = createAction('[Cocktail] Remove Request', props<{id: string}>());
export const removeCocktailSuccess = createAction('[Cocktail] Remove Success');
export const removeCocktailFailure = createAction('[Cocktail] Remove Failure', props<{error: string}>());
