import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { CocktailsService } from '../../services/cocktails.service';
import { catchError, map, mergeMap, of, tap } from 'rxjs';
import {
  createCocktailFailure,
  createCocktailRequest,
  createCocktailSuccess,
  fetchCocktailsFailure,
  fetchCocktailsRequest,
  fetchCocktailsSuccess,
  fetchUserCocktailsFailure,
  fetchUserCocktailsRequest,
  fetchUserCocktailsSuccess,
  publishCocktailRequest,
  publishCocktailSuccess,
  removeCocktailFailure,
  removeCocktailRequest,
  removeCocktailSuccess
} from './cocktails.actions';
import { Router } from '@angular/router';
import { HelpersService } from '../../services/helpers.service';

@Injectable()
export class CocktailsEffects {
  constructor(
    private actions: Actions,
    private cocktailsService: CocktailsService,
    private router: Router,
    private helpers: HelpersService,
  ) {}

  fetchCocktails = createEffect(() => this.actions.pipe(
    ofType(fetchCocktailsRequest),
    mergeMap(() => this.cocktailsService.getCocktails().pipe(
      map( cocktails => fetchCocktailsSuccess({cocktails})),
      catchError(() => of(fetchCocktailsFailure({error: 'Something wrong'})))
    ))
  ));

  fetchUserCocktails = createEffect(() => this.actions.pipe(
    ofType(fetchUserCocktailsRequest),
    mergeMap(({id}) => this.cocktailsService.getUserCocktails(id).pipe(
      map(userCocktails => fetchUserCocktailsSuccess({userCocktails})),
      catchError(() => of(fetchUserCocktailsFailure({error: 'Smth wrong'})))
    ))
  ));

  createCocktail = createEffect(() => this.actions.pipe(
    ofType(createCocktailRequest),
    mergeMap(({cocktailData}) => this.cocktailsService.createCocktail(cocktailData).pipe(
      map(() => createCocktailSuccess()),
      tap(() => {this.helpers.openSnackbar('Спасибо, ожидайте проверки!');
        void this.router.navigate(['/cocktails']);
      }),
      catchError(() => of(createCocktailFailure({error: 'Wrong data'})))
    ))
  ));

  publishCocktail = createEffect(() => this.actions.pipe(
    ofType(publishCocktailRequest),
    mergeMap(({id, change}) => this.cocktailsService.publishCocktail(id, change).pipe(
      map(cocktail => publishCocktailSuccess({cocktail}))
    ))
  ));

  removeCocktails = createEffect(() => this.actions.pipe(
    ofType(removeCocktailRequest),
    mergeMap(({id}) => this.cocktailsService.removeCocktail(id).pipe(
      map(() => removeCocktailSuccess()),
      catchError(() => of(removeCocktailFailure({error: 'Something wrong'})))
    ))
  ));
}
