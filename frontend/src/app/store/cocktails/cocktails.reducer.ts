import { CocktailsState } from '../types';
import {
  createCocktailFailure,
  createCocktailRequest,
  createCocktailSuccess,
  fetchCocktailsFailure,
  fetchCocktailsRequest,
  fetchCocktailsSuccess,
  fetchUserCocktailsRequest,
  fetchUserCocktailsSuccess,
  publishCocktailRequest,
  publishCocktailSuccess,
  removeCocktailFailure,
  removeCocktailSuccess,
} from './cocktails.actions';
import { createReducer, on } from '@ngrx/store';

const initialState: CocktailsState = {
  cocktails: [],
  userCocktails: [],
  fetchLoading: false,
  fetchError: null,
  fetchUserCocktailsLoading: false,
  fetchUserCocktailsError: null,
  createLoading: false,
  createError: null,
  publishLoading: false,
  publishError: null,
  deleteLoading: false,
  deleteError: null
}

export const cocktailsReducer = createReducer(
  initialState,
  on(fetchCocktailsRequest, state => ({...state, fetchLoading: true})),
  on(fetchCocktailsSuccess, (state, {cocktails}) => ({...state, fetchLoading: false, cocktails})),
  on(fetchCocktailsFailure, (state, {error}) => ({...state, fetchLoading: false, fetchError: error})),

  on(fetchUserCocktailsRequest, state => ({...state, fetchUserCocktailsLoading: true})),
  on(fetchUserCocktailsSuccess, (state, {userCocktails}) => ({...state, fetchUserCocktailsLoading: false, userCocktails})),

  on(createCocktailRequest, state => ({...state, createLoading: true})),
  on(createCocktailSuccess, state => ({...state, createLoading: false})),
  on(createCocktailFailure, (state, {error}) => ({...state, createLoading: false, createError: error})),

  on(publishCocktailRequest, state => ({...state, changeLoading: true})),
  on(publishCocktailSuccess, (state, {cocktail}) => ({
    ...state,
    cocktails: state.cocktails.map(item => {
      if (item._id === cocktail._id) {
        return cocktail;
      }
      return item;
    })
  })),

  on(removeCocktailSuccess, state => ({...state, deleteLoading: true})),
  on(removeCocktailSuccess, state => ({...state, deleteLoading: false})),
  on(removeCocktailFailure, (state, {error}) => ({...state, deleteLoading: false, deleteError: error})),
);
