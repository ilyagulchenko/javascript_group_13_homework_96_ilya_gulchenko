import { LoginError, User } from '../models/user.model';
import { Cocktail, CocktailDataApi, CocktailId } from '../models/cocktail.model';

export type UsersState = {
  user: null | User,
  loginLoading: boolean,
  loginError: null | LoginError,
};

export type CocktailsState = {
  cocktails: Cocktail[],
  userCocktails: CocktailDataApi[],
  fetchLoading: boolean,
  fetchError: null | string,
  fetchUserCocktailsLoading: boolean,
  fetchUserCocktailsError: null | string,
  createLoading: boolean,
  createError: null | string,
  publishLoading: boolean,
  publishError: null | string,
  deleteLoading: boolean,
  deleteError: null | string,
};

export type CocktailState = {
  cocktail: null | CocktailId,
  getLoading: boolean,
  getError: null | string,
}

export type AppState = {
  users: UsersState,
  cocktails: CocktailsState,
  cocktail: CocktailState
};
