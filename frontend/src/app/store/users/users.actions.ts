import { createAction, props } from '@ngrx/store';
import { LoginError, LoginFacebookUser, LoginUserData, User } from '../../models/user.model';

export const loginUserRequest = createAction('[Users] Login Request', props<{userData: LoginUserData}>());
export const loginUserSuccess = createAction('[Users] Login Success', props<{user: User}>());
export const loginUserFailure = createAction('[Users] Login Failure', props<{error: null | LoginError}>());

export const facebookLoginUserRequest = createAction('[Users] Facebook Login Request', props<{userData: LoginFacebookUser}>());

export const logoutUser = createAction('[Users] Logout');

export const logoutUserRequest = createAction('[Users] Server Logout Request');
