import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  facebookLoginUserRequest,
  loginUserFailure,
  loginUserRequest,
  loginUserSuccess,
  logoutUser,
  logoutUserRequest
} from './users.actions';
import { map, mergeMap, tap } from 'rxjs';
import { UsersServices } from '../../services/users.services';
import { HelpersService } from '../../services/helpers.service';
import { Router } from '@angular/router';

@Injectable()
export class UsersEffects {

  constructor(
    private actions: Actions,
    private usersService: UsersServices,
    private helpers: HelpersService,
    private router: Router,
  ) {}

  loginUser = createEffect(() => this.actions.pipe(
    ofType(loginUserRequest),
    mergeMap(({userData}) => this.usersService.login(userData).pipe(
      map(user => loginUserSuccess({user})),
      tap(() => {this.helpers.openSnackbar('Login successful');
        void this.router.navigate(['/']);
      }),
      this.helpers.catchServerError(loginUserFailure)
    ))
  ));

  facebookLogin = createEffect(() => this.actions.pipe(
    ofType(facebookLoginUserRequest),
    mergeMap(({userData}) => this.usersService.facebookLogin(userData).pipe(
      map(user => loginUserSuccess({user})),
      tap(() => {this.helpers.openSnackbar('Facebook login successful');
        void this.router.navigate(['/']);
      }),
      this.helpers.catchServerError(loginUserFailure)
    ))
  ));

  logoutUser = createEffect(() => this.actions.pipe(
    ofType(logoutUserRequest),
    mergeMap(() => {
      return this.usersService.logout().pipe(
        map(() => logoutUser()),
        tap(async () => {
          await this.router.navigate(['/']);
          this.helpers.openSnackbar('Logout successful');
        })
      );
    })
  ));
}
